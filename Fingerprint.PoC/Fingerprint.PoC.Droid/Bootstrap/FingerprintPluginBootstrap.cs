﻿using MvvmCross.Platform.Plugins;

namespace Fingerprint.PoC.Droid.Bootstrap
{
    public class FingerprintPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Fingerprint.PluginLoader>
    {
    }
}