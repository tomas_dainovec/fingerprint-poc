﻿using MvvmCross.Platform.Plugins;

namespace Fingerprint.PoC.iOS.Bootstrap
{
    public class FingerprintPluginBootstrap
        : MvxLoaderPluginBootstrapAction<MvvmCross.Plugins.Fingerprint.PluginLoader, MvvmCross.Plugins.Fingerprint.iOS.Plugin>
    {
    }
}