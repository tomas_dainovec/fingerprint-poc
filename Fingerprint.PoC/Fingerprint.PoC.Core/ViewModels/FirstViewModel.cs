﻿using System.Windows.Input;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using Plugin.Fingerprint.Abstractions;

namespace Fingerprint.PoC.Core.ViewModels
{
    public class FirstViewModel : MvxViewModel
    {
        private readonly IFingerprint fpService;

        public FirstViewModel(IFingerprint fpService)
        {
            this.fpService = fpService;
        }

        public async override void ViewAppeared()
        {
            while(true)
            {
                var result = await fpService.AuthenticateAsync(new AuthenticationRequestConfiguration("Authorise") { UseDialog = false });
                if (result.Authenticated)
                {
                    await Application.Current.MainPage.DisplayAlert("Fingerprint", "Authorised", "OK");
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Fingerprint", "Access Denied", "OK");
                }
            }
        }
    }
}
