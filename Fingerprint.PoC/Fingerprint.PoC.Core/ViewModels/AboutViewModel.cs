﻿using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using Plugin.Fingerprint.Abstractions;

namespace Fingerprint.PoC.Core.ViewModels
{
    public class AboutViewModel : MvxViewModel
    {

        public async override void ViewAppeared()
        {
            var fpService = Mvx.Resolve<IFingerprint>();

            //var result = await fpService.AuthenticateAsync("Prove you have mvx fingers!");
            var result = await fpService.AuthenticateAsync(new AuthenticationRequestConfiguration("Prove you have mvx fingers!") { UseDialog = false });
            if (result.Authenticated)
            {
                await Application.Current.MainPage.DisplayAlert("Fingerprint", "Authorised", "OK");
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Fingerprint", "Access Denied", "OK");
            }
        }

    }
}
